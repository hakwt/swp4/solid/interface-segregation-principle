# Interface Segregation Principle

Im Package `at.hakwt.swp4.sip.violated` findet man die Struktur einer kleinen Anwendung (ohne genauerer Implementierung).
Der Entwickler hat sich entschieden für die Klassen `BicycleGarage` und `CarGarage` das gemeinsame Interface `Garage`
einzuführen. Die Idee dahinter war, dass Verwender von Fahrrad- bzw. Autowerkstätten nur mit einer generellen Werkstatt
(=`Garage`) umgehen müssen.

Mittlerweile haben wir aber vom __Interface Segregation Principle__ gelernt und wollen dieses Wissen einbringen. 
Verbessere die Situation in diesem Repository, commite und pushe die Lösung.