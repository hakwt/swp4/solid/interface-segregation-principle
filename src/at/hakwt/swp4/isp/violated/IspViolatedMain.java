package at.hakwt.swp4.isp.violated;

public class IspViolatedMain {

    public static void main(String[] args) {
	    Garage garage = new BicycleGarage();

        Cyclist cyclist = new Cyclist();
        cyclist.buyRaceBike(garage.sell());
        
        Garage carGarage = new CarGarage();
        Traveler traveler = new Traveler(new Car());
        carGarage.maintain(traveler.getCar());
    }
}
