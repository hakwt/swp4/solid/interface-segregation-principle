package at.hakwt.swp4.isp.violated;

public class Traveler {

    private final Car car;

    public Traveler(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }
}
