package at.hakwt.swp4.isp.violated;

public class BicycleGarage implements Garage {
    @Override
    public void maintain(Car car) {
        throw new UnsupportedOperationException("No clue how to maintain a car");
    }

    @Override
    public void changeEngine(Car car) {
        throw new UnsupportedOperationException("No clue how to change the engine of a car");
    }

    @Override
    public void maintain(Bicycle bicycle) {
        // do bicycle maintenance work
    }

    @Override
    public Bicycle sell() {
        return new Bicycle();
    }
}
